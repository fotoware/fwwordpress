<?php


class FotoWeb_Options {

	public $options;
	public $options_name = 'fotoweb_plugin_options';

	public function __construct () {
		// delete_option('fotoweb_plugin_options');
		add_action('admin_init', array($this, 'register_settings_and_fields'));
        add_action('admin_menu', array($this, 'add_menu_page'));
		$this->options = get_option( $this->options_name);
	}


	public function add_menu_page() {
		add_options_page( 'FotoWeb', 'FotoWeb', 'administrator', __FILE__, array($this, 'display_options_page') );
	}


	public function display_options_page () {
		?>
			<div class="wrap">
				<form method="post" action="options.php">
					<?php 
						settings_fields($this->options_name);
						do_settings_sections(__FILE__); 
						submit_button();
					?>
				</form>
			</div>
		<?php
	}


	public function register_settings_and_fields () {
		register_setting($this->options_name, $this->options_name, array($this, 'fotoweb_validate_settings')); //3rd param = optional callback
		add_settings_section( 'fw_main_section', 'FotoWeb Settings', array($this, 'fw_main_section_cb'), __FILE__ );
		add_settings_field( 'fw_url', 'URL', array($this, 'fw_url_setting'), __FILE__, 'fw_main_section');
		add_settings_field( 'fw_crop_width', 'Crop Width', array($this, 'fw_crop_width_setting'), __FILE__, 'fw_main_section');
		add_settings_field( 'fw_crop_height', 'Crop Height', array($this, 'fw_crop_height_setting'), __FILE__, 'fw_main_section');
		add_settings_field( 'fw_publication_text', 'Publication Text', array($this, 'fw_publication_text_setting'), __FILE__, 'fw_main_section');
		add_settings_field( 'fw_preset_name', 'Preset Name', array($this, 'fw_preset_name_setting'), __FILE__, 'fw_main_section');
	}


	// TODO: Add validationr
	public function fotoweb_validate_settings ($plugin_options) {
		return $plugin_options;
	}


	public function fw_main_section_cb () {
		// optional
	}


	/*
	*
	* INPUTS
	*
	*/
	public function fw_url_setting () {
		echo "<input name='$this->options_name[fw_url]' type='text' value='{$this->options['fw_url']}' />";
	}

	public function fw_crop_width_setting () {
		echo "<input name='$this->options_name[fw_crop_width]' type='text' value='{$this->options['fw_crop_width']}' />";
	}

	public function fw_crop_height_setting () {
		echo "<input name='$this->options_name[fw_crop_height]' type='text' value='{$this->options['fw_crop_height']}' />";
	}

	public function fw_publication_text_setting () {
		echo "<input name='$this->options_name[fw_publication_text]' type='text' value='{$this->options['fw_publication_text']}' />";
	}

	public function fw_preset_name_setting () {
		echo "<input name='$this->options_name[fw_preset_name]' type='text' value='{$this->options['fw_preset_name']}' />";
	}
}

