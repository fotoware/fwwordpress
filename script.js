
function listener (event) {

	var url = fwParams.fw_url; //fwParams is created with php
	if (event.origin !== url)
		return;

	if (event.data === 'authenticated') {
		var iframes = document.getElementsByTagName('iframe');
		var FW_iframe = '';
		for (var i in iframes) {
			if (iframes[i].src.match(fwParams.fw_url)) {
				iframes[i].src = iframes[i].src;
				break;
			}
		}
	} else if (event.data.event === 'assetSelected') {
		handleSelected(getFrameURL(event.data, url));
	} else if (event.data.event === 'assetExported') {
		handleExported(event.data, url);
	}
}

function handleSelected(url) {
	setTimeout(function () {
		tb_show("FotoWeb", url);
	}, 300);
}

function handleExported (data, url) {
	wp.media.editor.insert('<img class="alignnone size-medium wp-image-14" src="' + data.export.export.image.normal + 
		'" alt=""'+
		'" width="' + data.export.export.size.w +
		'" height="' + data.export.export.size.h +
		'" />');
	tb_remove();
}

function getFrameURL (data, url) {
	var frameURL = url + '/fotoweb/widgets/publish?i=' + encodeURIComponent(data.asset.href);

	if (fwParams.fw_preset_name) {
		frameURL += '&p=' + fwParams.fw_preset_name;
	} else {
		if (fwParams.fw_crop_width > 0  && fwParams.fw_crop_height > 0) {
			frameURL += '&w=' + fwParams.fw_crop_width + '&h=' + fwParams.fw_crop_height
		}

		if (fwParams.fw_publication_text) {
			frameURL += '&pub=' + fwParams.fw_publication_text;
		}
	}

	frameURL += '&TB_iframe=true&width=600&height=550';

	return frameURL;
}

if (window.addEventListener) {
	addEventListener('message', listener, false);
}



