<?php
/**
 * Plugin Name: FotoWeb
 * Plugin URI: http://www.fotoware.com
 * Description: A Plugin to insert images from FotoWeb into Wordpress
 * Version: 0.3
 * Author: FotoWare
 * Author URI: http://www.fotoware.com
 * License: GPL2
 */


defined('ABSPATH') or die("No script kiddies please!");

include( plugin_dir_path( __FILE__ ) . 'options.php');

new FotoWeb();

class FotoWeb {

	public $fw_options;

	public function __construct () {
		$this->fw_options = new FotoWeb_Options();
		if($this->fw_options->options['fw_url']) {
			add_action('media_buttons', array($this,'add_fotoweb_button'), 15);
			add_action('wp_enqueue_media', array($this,'register_my_scripts'));
		}
	}

	public function add_fotoweb_button() {
	    echo '<a href="' . $this->fw_options->options['fw_url'] . '/fotoweb/widgets/selection?TB_iframe=true&width=600&height=550" class="button thickbox">Add from FotoWeb</a>';
	}

	public function register_my_scripts() {
		wp_enqueue_script('fw-javascript', plugins_url( 'script.js', __FILE__ ), array('jquery'), true);
		
		// Make fwParams available in Javascript
		wp_localize_script( 'fw-javascript', 'fwParams', $this->fw_options->options );
	}
}

